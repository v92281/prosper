# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    "name": 'Point Of Sale Total Price Without Text',
    "version": '1.0',
    "description": """
        Change total price without tax
    """,
    "author": 'Prosper Guena',
    "website": '',
    "category": "cpg-consulting.com",
    'images': [],
    "depends": [
        'base',
        'point_of_sale',
    ],
    "init_xml": [],
    'data': [
        'views/pos_config_view.xml',
        'views/templates.xml',
        ],
    "qweb": ['static/src/xml/pos.xml'],
    "test": [],
    "demo_xml": [],
    "installable": True,
    'auto_install': False,
    'license': 'LGPL-3',
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
