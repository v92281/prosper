# -*- coding: utf-8 -*-

from odoo import api, models


class PosOrderLine(models.Model):
    _inherit = 'pos.order.line'

    @api.depends('price_unit', 'tax_ids', 'qty', 'discount', 'product_id')
    def _compute_amount_line_all(self):
        super(PosOrderLine, self)._compute_amount_line_all()
        for line in self:
            config_id = line.order_id.session_id.config_id
            fpos = line.order_id.fiscal_position_id
            tax_ids_after_fiscal_position = fpos.map_tax(
                line.tax_ids, line.product_id,
                line.order_id.partner_id) if fpos else line.tax_ids
            price = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
            taxes = tax_ids_after_fiscal_position.compute_all(
                price, line.order_id.pricelist_id.currency_id,
                line.qty, product=line.product_id,
                partner=line.order_id.partner_id)
            taxes_difference = taxes['total_included'] - taxes['total_excluded']
            line.update({
                'price_subtotal_incl': (taxes['total_excluded'] - taxes_difference) if config_id.ftotal_price_without_tax else taxes['total_included'],
                'price_subtotal': taxes['total_excluded'],
            })


class PosOrder(models.Model):
    _inherit = 'pos.order'

    @api.model
    def _amount_line_tax(self, line, fiscal_position_id):
        res = super(PosOrder, self)._amount_line_tax(line, fiscal_position_id)
        config_id = line.order_id.session_id.config_id
        if config_id.ftotal_price_without_tax:
            res = res * -1
        return res

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
