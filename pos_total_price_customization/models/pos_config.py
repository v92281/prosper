# -*- coding: utf-8 -*-

from odoo import fields, models


class PosConfig(models.Model):
    _inherit = 'pos.config'

    ftotal_price_without_tax = fields.Boolean(
        string='Total Price Without Tax',
        help='The displayed total prices without tax')

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
